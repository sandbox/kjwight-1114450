<?php //print_r($node) ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <!--[if lt IE 7]>
  <link rel="stylesheet" type="text/css" media="all" href="<?php print $base_path . $directory; ?>/ie.css" />
  <![endif]-->
  <?php print $head; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $body_classes; ?>">
  <div id="pagewrapper">
    <div id="fip">
      <div id="banner" <?php if ($logo != '') { print " style=\"background-image: url($logo)\""; } ?>>
      <div id="banner-text"><p class="site-name"><?php print $site_name ?></p>
      <p class="site-slogan"><?php print $site_slogan; ?></p></div>
      </div>
      <div id="common-menu">
        <?php print theme('links', $primary_links); ?>
      </div>
    </div>
    <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <div id="container">
      <div id="left"><?php print $left; ?></div>
      <?php if (!empty($right)): ?>
      <div id="right"><?php print $right; ?></div>
      <?php endif; ?>
      <div id="center">
       <?php if (!empty($tabs)): ?>
       <?php echo $tabs; ?>
       <?php endif; ?>

       <?php if (!empty($messages)): ?>
       <?php echo $messages; ?>
       <?php endif; ?>

       <?php if (!empty($help)): ?>
       <?php echo $help; ?>
       <?php endif; ?>

       <?php if (!empty($mission)): ?>
       <?php echo $mission; ?>
       <?php endif; ?>

       <?php echo $content; ?>
      </div>
    </div>
    <div id="footer">
      <div id="footer-left">Last Update: <?php print ulf_content_last_modified($node); ?></div>
      <div id="footermsg"><?php print $footer_message; ?></div>
    </div>
	  <div style="clear: both;">&nbsp;</div>
  </div>
  </div>
  <?php print $closure; ?>
</body>
</html>
