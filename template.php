<?php

/**
 * Last modified date of the current document
 */
function ulf_content_last_modified($node, $format = 'd-m-Y') {
  $output = '';

  if (is_null($node->changed)) {
  if (is_null($node->created)) {
    $output .= format_date(time(), 'custom', $format);
  } else {
    $output .= format_date($node->created, 'custom', $format);
  }
  } else {
    $output .= format_date($node->changed, 'custom', $format);
  }
  return $output;
}

/**
 * Browser check
 */
function ulf_browser_detect() {
  $browser = 'none';
  if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari')) {
    $browser = 'safari';
  } else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Gecko')) {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'Netscape')) {
      $browser = 'netscape';
    } else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox')) {
      $browser = 'firefox';
    } else {
      $browser = 'mozilla';
    }
  } else if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
    $browser = 'ie';
  } else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Opera') === TRUE) {
    $browser = 'opera';
  }

  return $browser;
}

/**
 * Test if the user is running IE
 */
function ulf_is_ie() {
  $browser = ulf_browser_detect();
  $is_ie = FALSE;

  if ($browser == 'ie') {
    $is_ie = TRUE;
  }

  return $is_ie;
}
?> 
