ULF Readme v1.0

K.J. Wight <kjwight@nfinitek.ca>
Even though he didn't want any props, thanks to Reg Maltais for his contributions to this project.

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
LICENSE:
This theme is licensed under the GNU GPL v2.

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
CHANGELOG:
02-Apr-2011: v1.0 written

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Installation Notes:

Unzip/untar the theme file and copy all contents to your Drupal 6.x install path under sites/all/themes.

On the administration page, click Themes under Site Building.

Check the "Enabled" checkbox, and click the "Default" radio button.

Scroll to the bottom of the page and click "Save".

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Usage Notes:

If you are using a custom logo on your site:
Image dimensions for this theme are 760px wide, 80px high. I have included a template .psd file with the theme to make this easier.

Using your new logo:

Either use Site Building > Themes > Configure > Logo image settings to select your new logo.

-or-

Overwrite logo.png (in the sites/all/themes/ulf directory) with your new banner.

If you have your site name in your logo image:

Go to the administration page, and under "Site Building" > "Themes" > "ULF" > "Configure", uncheck "Site Name". 
Your Site's Name (text) will not be displayed on the banner now.

The "Common-Menu" Link Bar:

This Link Bar contains your Primary Links. Overflow is hidden, so if you have too many primary links, they don't show up!

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
