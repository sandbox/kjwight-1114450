<div id="node-<?php echo $node->nid; ?>" class="node<?php if ($sticky) { echo ' sticky'; } ?><?php if (!$status) { echo ' node-unpublished'; } ?>">

  <?php

  if (empty($node->field_banner_image[0]->view)) {
    echo '<h1 class="page-title">' . $title . '</h1>';
  }
  ?>

  <div class="content clear-block">
    <?php echo $content; ?>
  </div>

  <div class="clear-block">
    <?php if ($links): ?>
      <div class="links"><?php echo $links; ?></div>
    <?php endif; ?>
  </div>

</div>
